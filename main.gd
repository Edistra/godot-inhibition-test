extends Node2D

var mode = "title_screen"
var pause_menu = null
var is_paused = false
var delay = 1
var current_node = null
var container = null
var fullscreen = false

func _ready():
	container = Node2D.new()
	add_child(container)

	pause_menu = load("res://pause_menu.tscn").instance()
	pause_menu.get_node("container/minus").connect("pressed", self, "decrease_delay")
	pause_menu.get_node("container/plus").connect("pressed", self, "increase_delay")
	pause_menu.layer = 2
	pause_menu.get_node("container").visible = false
	add_child(pause_menu)

	init("res://title.tscn")

func _process(delta):
	if mode == "title_screen" and Input.is_action_just_pressed("ui_select"):
		mode = "game"
		init("res://game.tscn")
	elif mode == "game" and Input.is_action_just_pressed("ui_cancel"):
		mode = "title_screen"
		init("res://title.tscn")
	
	if Input.is_action_just_pressed("ui_pause"):
		pause_menu.get_node("container").visible = not pause_menu.get_node("container").visible
		is_paused = not is_paused
		if mode == "game":
			current_node._timer.set_paused(is_paused)
	if Input.is_action_just_pressed("ui_fullscreen"):
		fullscreen = not fullscreen
		OS.set_window_fullscreen(fullscreen)

func init(path):
	clean()
	current_node = load(path).instance()
	current_node.layer = 1
	container.add_child(current_node)

func clean():
	for child in container.get_children():
		child.queue_free()

func decrease_delay():
	if delay > 1:
		delay -= 1
func increase_delay():
	if delay < 7:
		delay += 1
