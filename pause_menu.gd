extends CanvasLayer

onready var delay_label = find_node("delay_label")


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	if get_parent():
		delay_label.text = str(get_parent().delay)
