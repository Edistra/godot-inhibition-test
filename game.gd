extends CanvasLayer

var img_path = "res://img"
var imgs = []
var _timer = null
var delay = 3
var img_node = null
var current_index = -1
var positive_score = 0
var input_pressed = false
onready var score = find_node("score")
onready var result = find_node("result")
onready var time_left = find_node("time_left")

func _ready():
	img_path = get_path() + "/img"
	img_node = find_node("img")
	for path in list_files_in_directory(img_path):
		var texture = ImageTexture.new()
		texture.set_meta("name", path)
		var img = Image.new()
		img.load(img_path + "/" + path)
		texture.create_from_image(img)
		imgs.append(texture)

	if get_parent().get_parent():
		delay = get_parent().get_parent().delay
	
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	show_next_img()

func _process(delta):
	time_left.visible = not input_pressed
	time_left.set_scale(Vector2(_timer.get_time_left() / delay, 1))
	if get_parent().get_parent() and get_parent().get_parent().delay != delay:
		delay = get_parent().get_parent().delay
		_timer.set_wait_time(delay)

	if not input_pressed and Input.is_action_just_pressed("ui_select"):
		input_pressed = true
		if "pikachu" in imgs[current_index].get_meta("name"):
			img_node.shake(1, 30, 10)
		else:
			positive_score += 1
			img_node.fade_out()
		var new_delay = 1 if _timer.get_time_left() > 1 else _timer.get_time_left()
		reset_timer(new_delay)

func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with(".") and not file.ends_with(".import"):
			files.append(file)
	dir.list_dir_end()
	return files

func _on_Timer_timeout():
	if not input_pressed and "pikachu" in imgs[current_index].get_meta("name"):
		positive_score += 1
	input_pressed = false
	show_next_img()

func show_next_img():
	_timer.stop()
	img_node.stop_shake()
	img_node.stop_glow()
	current_index += 1
	if current_index >= len(imgs):
		result.bbcode_text = "[color=#000000][center]Bravo ! Tu as %s bonnes réponses sur %s [/center][/color]" % [positive_score, len(imgs)]
		img_node.set_texture(null)
		input_pressed = true
	else:
		img_node.set_texture(imgs[current_index])
		reset_timer(delay)

func reset_timer(d):
	_timer.set_wait_time(d)
	_timer.set_one_shot(true)
	_timer.start()

func get_path():
	var path = ProjectSettings.globalize_path("res://")
	if path == "": # Exported, will return the folder where the executable is located.
		path = OS.get_executable_path().get_base_dir()
	else: # Editor, will return one level above the project folder
		path = path.get_base_dir()
	return path